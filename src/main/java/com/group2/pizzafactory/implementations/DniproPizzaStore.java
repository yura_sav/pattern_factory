package com.group2.pizzafactory.implementations;

import com.group2.PizzaCollection;
import com.group2.pizzafactory.PizzaStore;
import com.group2.pizzassuppliers.PizzaSupplier;
import com.group2.pizzassuppliers.implementations.julienne.juliennecity.DniproJulienneSupplier;
import com.group2.pizzassuppliers.implementations.meatplatter.meatplattercity.DniproMeatPlatterSupplier;
import com.group2.pizzassuppliers.implementations.peperoni.peperonicity.DniproPeperoniSupplier;
import com.group2.pizzassuppliers.implementations.proscuittocongunghi.proscuittoconfunghicity.DniproProsciuttoConFunghiSupplier;
import com.group2.pizzassuppliers.implementations.vegeteriano.vegeterianocity.DniproVegeterianoSupplier;

public class DniproPizzaStore extends PizzaStore {

    @Override
    protected PizzaSupplier createPizzaSupplier(PizzaCollection pizza, int size, double thickness) {
        PizzaSupplier supplier = null;

        if (pizza == PizzaCollection.PEPERONI) {
            supplier = new DniproPeperoniSupplier(size, thickness);
        } else if (pizza == PizzaCollection.JULIENNE) {
            supplier = new DniproJulienneSupplier(size, thickness);
        } else if (pizza == PizzaCollection.PROSCIUTTOCONFUNGHI) {
            supplier = new DniproProsciuttoConFunghiSupplier(size, thickness);
        } else if (pizza == PizzaCollection.MEATPLATTER) {
            supplier = new DniproMeatPlatterSupplier(size, thickness);
        } else if (pizza == PizzaCollection.VEGETERIANO) {
            supplier = new DniproVegeterianoSupplier(size, thickness);
        }

        return supplier;
    }
}
