package com.group2.pizzassuppliers;

import com.group2.pizzassuppliers.pizza.Pizza;

import java.math.BigDecimal;

public interface PizzaSupplier {
    void prepare();

    void bake();

    void cut();

    void box();

    void send();

    BigDecimal getPrice();

    Pizza givePizza();
}
