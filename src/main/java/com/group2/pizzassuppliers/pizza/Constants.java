package com.group2.pizzassuppliers.pizza;

public class Constants {

    private Constants() {
    }

    public static final int USUAL_BAKING_TIME = 2500;
    public static final int USUAL_ADDING_TIME = 1300;
    public static final int USUAL_CUTTING_TIME = 1400;
    public static final int USUAL_BOXING_TIME = 1500;
}
