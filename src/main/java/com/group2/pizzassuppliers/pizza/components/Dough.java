package com.group2.pizzassuppliers.pizza.components;

import java.math.BigDecimal;

public class Dough extends Component {
    private double thickness;

    public Dough(String name, BigDecimal price, double thickness) {
        this.setName(name);
        this.thickness = thickness;
        this.setPrice(price.multiply(new BigDecimal(thickness)));
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    @Override
    public String toString() {
        return getName() + " with thickness " + getThickness();
    }
}
