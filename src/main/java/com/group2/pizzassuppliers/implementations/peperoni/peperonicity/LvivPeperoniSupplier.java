package com.group2.pizzassuppliers.implementations.peperoni.peperonicity;

import com.group2.pizzassuppliers.implementations.peperoni.Peperoni;
import com.group2.pizzassuppliers.pizza.Constants;
import com.group2.pizzassuppliers.pizza.Pizza;
import com.group2.pizzassuppliers.pizza.components.Topping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;

public class LvivPeperoniSupplier extends Peperoni {
    private static final Logger logger = LogManager.getLogger(LvivPeperoniSupplier.class);

    public LvivPeperoniSupplier(int size, double thickness) {
        super(size, thickness, Optional.ofNullable(getProperties().
                        getProperty("LVIV_PEPERONI_BAKING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BAKING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("LVIV_PEPERONI_ADDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_ADDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("LVIV_PEPERONI_CUTTING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_CUTTING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("LVIV_PEPERONI_BOXING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BOXING_TIME));
        components.add(new Topping("Lviv secret ingredient", new BigDecimal(10)));
        pizza = new Pizza("Peperoni[Lviv]", "PEPPERONI", components);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = LvivPeperoniSupplier.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
