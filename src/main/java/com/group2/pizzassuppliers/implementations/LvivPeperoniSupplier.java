package com.group2.pizzassuppliers.implementations;

import com.group2.pizzassuppliers.PizzaSupplier;
import com.group2.pizzassuppliers.pizza.Pizza;
import com.group2.pizzassuppliers.pizza.components.Component;
import com.group2.pizzassuppliers.pizza.components.Dough;
import com.group2.pizzassuppliers.pizza.components.Sauce;
import com.group2.pizzassuppliers.pizza.components.Topping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LvivPeperoniSupplier implements PizzaSupplier {
    private static final Logger logger = LogManager.getLogger(LvivPeperoniSupplier.class);
    Pizza pizza;

    public LvivPeperoniSupplier(int size,double thickness){
        List<Component> components= new ArrayList<>();
        components.add(new Dough("Regular dough",new BigDecimal(50*(1+size*0.25)),thickness));
        components.add(new Sauce("Pesto",new BigDecimal(15.5)));
        components.add(new Topping("Pepperoni", new BigDecimal(11.3)));
        components.add(new Topping("Onions",new BigDecimal(8.85)));
        components.add(new Topping("Black olives",new BigDecimal(11.5)));
        components.add(new Topping("Lviv secret ingredient",new BigDecimal(10)));
        pizza=new Pizza("Peperoni[Lviv]","Pepperoni",components);
    }

    @Override
    public void prepare() {
        logger.info("Praparing 'Peperoni' pizza:");
        for(Component c:pizza.getComponents()){
            logger.info("Adding "+c.toString()+"...");
            try {
                Thread.sleep(1300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void bake() {
        logger.info("Baking...");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cut() {
        logger.info("Cutting...");
        try {
            Thread.sleep(1300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void box() {
        logger.info("Boxing...");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send() {
        logger.info("Your pizza was sent and should be delivered in up to 30 minutes.");
    }

    @Override
    public BigDecimal getPrice() {
        return pizza.getPrice();
    }

    @Override
    public Pizza givePizza() {
        return pizza;
    }
}
