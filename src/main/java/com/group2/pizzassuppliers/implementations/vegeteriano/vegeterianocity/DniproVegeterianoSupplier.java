package com.group2.pizzassuppliers.implementations.vegeteriano.vegeterianocity;

import com.group2.pizzassuppliers.implementations.vegeteriano.Vegeteriano;
import com.group2.pizzassuppliers.pizza.Constants;
import com.group2.pizzassuppliers.pizza.Pizza;
import com.group2.pizzassuppliers.pizza.components.Topping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;

public class DniproVegeterianoSupplier extends Vegeteriano {
    private static final Logger logger = LogManager.getLogger(DniproVegeterianoSupplier.class);

    public DniproVegeterianoSupplier(int size, double thickness) {
        super(size, thickness, Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_VEGETERIANO_BAKING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BAKING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_VEGETERIANO_ADDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_ADDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_VEGETERIANO_CUTTING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_CUTTING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_VEGETERIANO_BOXING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BOXING_TIME));
        components.add(new Topping("Dnipro secret ingredient", new BigDecimal(12)));
        pizza = new Pizza("Vegeteriano[Dnipro]", "VEGGIE", components);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DniproVegeterianoSupplier.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
