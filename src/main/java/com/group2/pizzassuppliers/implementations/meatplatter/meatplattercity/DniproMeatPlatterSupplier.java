package com.group2.pizzassuppliers.implementations.meatplatter.meatplattercity;

import com.group2.pizzassuppliers.implementations.meatplatter.MeatPlatter;
import com.group2.pizzassuppliers.pizza.Constants;
import com.group2.pizzassuppliers.pizza.Pizza;
import com.group2.pizzassuppliers.pizza.components.Topping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;

public class DniproMeatPlatterSupplier extends MeatPlatter {
    private static final Logger logger = LogManager.getLogger(DniproMeatPlatterSupplier.class);

    public DniproMeatPlatterSupplier(int size, double thickness) {
        super(size, thickness, Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_MEAT_PLATTER_BAKING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BAKING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_MEAT_PLATTER_ADDING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_ADDING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_MEAT_PLATTER_CUTTING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_CUTTING_TIME),
                Optional.ofNullable(getProperties().
                        getProperty("DNIPRO_MEAT_PLATTER_BOXING_TIME")).
                        map(Integer::valueOf).
                        orElse(Constants.USUAL_BOXING_TIME));
        components.add(new Topping("Dnipro secret ingredient", new BigDecimal(12)));
        pizza = new Pizza("Meat Platter[Dnipro]", "MEAT", components);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DniproMeatPlatterSupplier.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
