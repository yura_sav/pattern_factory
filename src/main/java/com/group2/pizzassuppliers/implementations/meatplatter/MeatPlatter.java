package com.group2.pizzassuppliers.implementations.meatplatter;

import com.group2.pizzassuppliers.PizzaSupplier;
import com.group2.pizzassuppliers.pizza.Pizza;
import com.group2.pizzassuppliers.pizza.components.Component;
import com.group2.pizzassuppliers.pizza.components.Dough;
import com.group2.pizzassuppliers.pizza.components.Sauce;
import com.group2.pizzassuppliers.pizza.components.Topping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MeatPlatter implements PizzaSupplier {
    private final int BAKING_TIME;
    private final int ADDING_TIME;
    private final int CUTTING_TIME;
    private final int BOXING_TIME;
    private static final Logger logger = LogManager.getLogger(MeatPlatter.class);
    protected Pizza pizza;
    protected List<Component> components;

    public MeatPlatter(int size, double thickness, int bakingTime, int addingTime, int cuttingTime, int boxingTime) {
        components = new ArrayList<>();
        components.add(new Dough("Regular dough", new BigDecimal(50 * (1 + size * 0.25)), thickness));
        components.add(new Sauce("'Tapenade'", new BigDecimal(11.5)));
        components.add(new Topping("sausage", new BigDecimal(20.85)));
        components.add(new Topping("Black olives", new BigDecimal(11.5)));
        BAKING_TIME = bakingTime;
        ADDING_TIME = addingTime;
        CUTTING_TIME = cuttingTime;
        BOXING_TIME = boxingTime;
    }

    @Override
    public void prepare() {
        logger.info("Preparing 'Meat Platter' pizza:");
        for (Component c : pizza.getComponents()) {
            logger.info("Adding " + c.toString() + "...");
            try {
                Thread.sleep(ADDING_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void bake() {
        logger.info("Baking...");
        try {
            Thread.sleep(BAKING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cut() {
        logger.info("Cutting...");
        try {
            Thread.sleep(CUTTING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void box() {
        logger.info("Boxing...");
        try {
            Thread.sleep(BOXING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send() {
        logger.info("Your pizza was sent and should be delivered in up to 30 minutes.");
    }

    @Override
    public BigDecimal getPrice() {
        return pizza.getPrice();
    }

    @Override
    public Pizza givePizza() {
        return pizza;
    }
}
