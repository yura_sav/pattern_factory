package com.group2;

import com.group2.pizzafactory.PizzaStore;
import com.group2.pizzafactory.implementations.DniproPizzaStore;
import com.group2.pizzafactory.implementations.KyivPizzaStore;
import com.group2.pizzafactory.implementations.LvivPizzaStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);
    private static Map<Integer, PizzaStore> storeMap = new HashMap<>() {{
        put(1, new LvivPizzaStore());
        put(2, new KyivPizzaStore());
        put(3, new DniproPizzaStore());
    }};
    private static Map<Integer, PizzaCollection> pizzaMap = new HashMap<>() {{
        put(1, PizzaCollection.PEPERONI);
        put(2, PizzaCollection.JULIENNE);
        put(3, PizzaCollection.PROSCIUTTOCONFUNGHI);
        put(4, PizzaCollection.MEATPLATTER);
        put(5, PizzaCollection.VEGETERIANO);
    }};

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in).useLocale(Locale.US);

        logger.info("WELCOME TO OUR SHOP 'PIZZA TIME'!\nPlease chose yor city:\nLviv-1\nKyiv-2\nDnipro-3");
        int choice = input.nextInt();
        boolean flag = true;
        while (flag) {
            int size = 1;
            double thickness = 1;
            PizzaStore pizzaStore = storeMap.get(choice);
            logger.info("Now, please, choose a pizza:\n'Peperoni'-1\n'Julienne'-2\n" +
                    "'Prosciutto con funghi'-3\n'Meat Platter'-4\n'Vegeteriano'-5");
            choice = input.nextInt();
            logger.info("Please, choose size:\nsmall-1\nmedium-2\nlarge-3");
            size = input.nextInt();
            logger.info("Please choose dough thickness (1-2 cm):");
            thickness = input.nextDouble();
            if (thickness < 1) {
                thickness = 1;
            } else if (thickness > 2) {
                thickness = 2;
            }
            logger.info("Price= " + NumberFormat.getCurrencyInstance().format(pizzaStore.getPrice(pizzaMap.get(choice), size, thickness)) + "\nIs this acceptable to you?(type yes/no)");
            input.nextLine();
            String iAccept = input.nextLine();
            if (iAccept.equalsIgnoreCase("yes")) {
                pizzaStore.process(pizzaMap.get(choice), size, thickness);
            } else {
                logger.info("We are sincerely sorry... Here, try again:");
                continue;
            }
            logger.info("Do you want another pizza? (type yes/no)");
            String iWantMore = input.nextLine();
            if (iWantMore.equalsIgnoreCase("no")) {
                flag = false;
                logger.info("Good bye! :)");
            }
        }
    }
}
